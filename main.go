package main
​
import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
)
​
func main() {
	WordbyWordScan()
}
​
func WordbyWordScan() {
​
	path := os.Getenv("path")
	targetPath := os.Getenv("targetPath")
	separator := os.Getenv("separator")
​
​
	if path == "" {
		log.Fatal("path is not defined")
	}
​
	if targetPath == "" {
		log.Fatal("target path is not defined")
	}
​
	if separator == "" {
		log.Fatal("seperator is not defined")
	}
​
	file, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)
​
	f, err := os.OpenFile(targetPath,
		os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
​
	if err != nil {
		log.Println(err)
	}
​
	defer f.Close()
​
	for scanner.Scan() {
		name := scanner.Text()
		arrayOfStrings := strings.Split(strings.ReplaceAll(name," ",""), separator)
​
		listingId := arrayOfStrings[3]
		result := fmt.Sprintf(`CSUUID("%s"),`, listingId)
​
		if _, err := f.WriteString(result+ "\n"); err != nil {
			log.Println(err)
		}
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
}
