Field seperator 
========

Features
--------
* This project splits the file with given seperator and writes to given target.
* Allows you to convert data (such as ID) into the query into the appropriate format while typing a query.

Variables
--------
* Path (where to file read to be), targetPath (where to file write to be) and separator should be defined.
* path="a path" | targetPath="a target path" | separator=","
- go run main.go


Contributors
============================================
* **[Serhan Guney](https://gitlab.com/sguney)**
  * Code review
